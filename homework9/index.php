<?php
    $id = $_POST['id'] ?? null;
    $pdo = new PDO('mysql:host=localhost;port=3306;dbname=homework9', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statment = $pdo->prepare('SELECT * FROM commerce WHERE id= :topics');
    $statment->bindValue(':topics', $id);
    $statment->execute();

    $prduct = $statment->fetch(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>commerce</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
        include_once "blocks/header.php";
    ?>
    <main>
        <?php
            include_once "blocks/nav.php";
            include_once "blocks/content.php";
        ?>
    </main>
    <?php
        include_once "blocks/footer.php";
    ?>
</body>
</html>
